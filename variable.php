<?php
$a=2;
echo $a;
echo "<br>";

$MyName=3;
echo $MyName;
echo "<br>";

$_yourName=40.89;
echo $_yourName;
echo "<br>";

/*Invalid variable
$1name=5;
echo $1name;
*/

$_1name=5;
echo $_1name;
//valid variable cause start with (_)
echo "<br>";

/*Invalid variable cause starts with number
$4Site="not yet";
echo $4Site;
*/

$ourCode="we love to code in php.";
echo $ourCode;

?>