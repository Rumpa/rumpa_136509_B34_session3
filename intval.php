<?php
echo intval(42)."<br>";
echo intval(4.2)."<br>";
echo intval('42')."<br>";
echo intval('+42')."<br>";
echo intval('-42')."<br>";
echo intval(042)."<br>";
echo intval('042')."<br>";
echo intval(1e10)."<br>";
echo intval('1e10')."<br>";
echo intval(0x1A)."<br>";
echo intval(42000000)."<br>";
echo intval(420000000000000000000)."<br>";
echo intval('420000000000000000000')."<br>";
echo intval(42, 8)."<br>";
echo intval('42', 8)."<br>";
echo intval(array())."<br>";
echo intval(array('foo', 'bar'))."<br>";
?>